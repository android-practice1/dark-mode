# Dark Mode for Android 
`[Shared Preference]` `[SwitchCompat]`

Night Theme:
```java
AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES); // setting night theme
imageView.setImageResource(R.drawable.night); // image for night theme
```
Light/Day Theme:
```java
AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO); // setting light theme
imageView.setImageResource(R.drawable.day); // image for light theme
```
**Status** 
## TESTED AND OK
